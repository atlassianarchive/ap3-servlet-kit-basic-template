#set( $D = '$' )
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>${groupId}</groupId>
    <artifactId>${pluginFolder}</artifactId>
    <version>1.0</version>
    <packaging>atlassian-plugin</packaging>
    <name>${pluginName}</name>
    <description>The ${pluginName} Plugin</description>
    
    <dependencies>
        <dependency>
            <groupId>com.atlassian.pluginkit</groupId>
            <artifactId>servlet-kit</artifactId>
            <version>${D}{servletkit.version}</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>remotable-plugins-plugin-api</artifactId>
            <version>${D}{p3.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
                <version>1.2.16</version>
                <scope>provided</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <finalName>${D}{project.artifactId}</finalName>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-amps-plugin</artifactId>
                <version>4.1.4</version>
                <extensions>true</extensions>
                <configuration>
                    <extractDependencies>true</extractDependencies>
                    <containerVersion>${D}{p3.version}</containerVersion>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.6</source>
                    <target>1.6</target>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>com.atlassian.plugins</groupId>
                                    <artifactId>remotable-plugins-container</artifactId>
                                    <version>${D}{p3.version}</version>
                                    <classifier>standalone</classifier>
                                </artifactItem>
                            </artifactItems>
                            <stripVersion>true</stripVersion>
                            <outputDirectory>${D}{build.directory}</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <properties>
        <p3.version>${latestP3Version}</p3.version>
        <servletkit.version>${latestServletKitVersion}</servletkit.version>
    </properties>

</project>
